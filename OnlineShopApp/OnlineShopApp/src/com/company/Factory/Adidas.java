package com.company.Factory;

import com.company.Adapter.Price;

public class Adidas implements Item, Price {

    public String toString() {
        return "Adidas";
    }

    @Override
    public void make() {
        System.out.println("Adidas clothes");
    }

    @Override
    public double getPrice() {
        return 320;
    }
}
