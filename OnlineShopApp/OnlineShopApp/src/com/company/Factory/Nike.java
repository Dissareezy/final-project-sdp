package com.company.Factory;

import com.company.Adapter.Price;

public class Nike implements Item, Price {
    public String toString() {
        return "Nike";
    }

    @Override
    public void make() {
        System.out.println("Nike clothes");
    }

    @Override
    public double getPrice() {
        return 300;
    }
}
