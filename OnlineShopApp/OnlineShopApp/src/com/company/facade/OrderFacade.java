package com.company.facade;

import com.company.Adapter.AdapterImplementation;
import com.company.PayByPayPal;
import com.company.Payment;
import com.company.PaymentStrategy;

public class OrderFacade {

    private PaymentStrategy paymentStrategy=new PayByPayPal();

    public OrderFacade(PaymentStrategy paymentStrategy) {
        this.paymentStrategy = paymentStrategy;
    }

    private AdapterImplementation price=new AdapterImplementation();

    private Payment payment=new Payment(paymentStrategy);

    public OrderFacade() {
    }

    public void order(){
        //price.getPrice();
        payment.pay();


    //1 factory
    //2 packaging

    }

}
