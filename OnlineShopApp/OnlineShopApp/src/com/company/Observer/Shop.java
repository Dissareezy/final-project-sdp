package com.company.Observer;

import com.company.Factory.Item;

import java.util.ArrayList;
import java.util.HashMap;

public class Shop implements IObservable {

    private HashMap<Integer, String> products;
    private ArrayList<IObserver> customers;
    private Integer itemId = 0;

    public Shop() {
        products = new HashMap<>();
        products.put(1001, "dress");
        products.put(1002, "skirt");
        products.put(2001, "t-shirt");
        products.put(2002, "trousers");

        customers = new ArrayList<>();
    }

    @Override
    public void registerUser(IObserver customer) {
        customers.add(customer);

    }

    @Override
    public void removeUser(IObserver customer) {
        customers.remove(customer);
    }


    public void newItems(Item company, String item) {
        boolean isExist = products.containsValue(item);

        if (!isExist) {
            itemId = products.size() + 1;
            products.put(products.size() + 1, item);
            notifyAllCustomers(company);
        }
        //System.out.println(products.get(5));
    }


    @Override
    public void notifyAllCustomers(Item company) {
        for (IObserver customer : customers) {
            customer.update(products.get(itemId), company);
        }
    }
}
