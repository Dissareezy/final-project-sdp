package com.company.Observer;

import com.company.Factory.*;

import java.util.HashMap;

public class Customer implements IObserver {
    private String id;
    private String name;
   private  String phoneNumber;
    private String address;
    //private Item itemCompany;
    private   Dialog dialog;
    private Item itemCompany;





    public Customer(String name, String phoneNumber, String address, Item itemCompany) {

        this.name = name;
        this.phoneNumber = phoneNumber;
        this.address = address;
        this.itemCompany=itemCompany;

        /*if(itemCompany.equals(new Adidas())) {
            dialog = new AdidasDialog();
        }
        else if(itemCompany.equals(new Nike())) {
            dialog = new NikeDialog();
        }*/
    }

    @Override
    public String toString() {
        return "Customer{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", address='" + address + '\'' +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public void update(String item, Item company) {
        if(itemCompany.equals(company)) {
           // dialog.createItem();
            System.out.println("Dear " + name + ", new item(" + item+ ")"+ " from "+company+" have arrived");
        }
    }

    public Item getItemCompany() {
        return itemCompany;
    }

    public void setItemCompany(Item itemCompany) {
        this.itemCompany = itemCompany;
    }
}
