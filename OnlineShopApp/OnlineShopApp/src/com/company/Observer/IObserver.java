package com.company.Observer;

import com.company.Factory.Item;

import java.util.HashMap;

public interface IObserver {
    void update(String item, Item company);

}
