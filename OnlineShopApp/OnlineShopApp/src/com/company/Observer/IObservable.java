package com.company.Observer;

import com.company.Factory.Item;

public interface IObservable {
    void registerUser(IObserver customer);
    void removeUser(IObserver customer);
    void notifyAllCustomers(Item company);
}
