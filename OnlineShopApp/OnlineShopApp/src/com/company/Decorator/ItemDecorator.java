package com.company.Decorator;

import com.company.Adapter.Price;

public abstract class ItemDecorator implements Price {
    private Price price;

    public ItemDecorator(Price price) {
        this.price = price;
    }

    @Override
    public double getPrice() {
        return price.getPrice();
    }
}
