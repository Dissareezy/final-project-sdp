package com.company.Decorator.ConcreteDecorators;

import com.company.Adapter.Price;
import com.company.Decorator.ItemDecorator;

public class IndividualColor extends ItemDecorator {
    public IndividualColor(Price price) {
        super(price);
    }

    @Override
    public double getPrice() {
        return super.getPrice() + 45;
    }
}
