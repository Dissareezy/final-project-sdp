package com.company.Decorator.ConcreteDecorators;

import com.company.Adapter.Price;
import com.company.Decorator.ItemDecorator;

public class StickerDecorator extends ItemDecorator {
    public StickerDecorator(Price price) {
        super(price);
    }

    @Override
    public double getPrice() {
        return super.getPrice() + 70;
    }
}
