package com.company;

public class Payment {
    private PaymentStrategy strategy;

    public Payment(PaymentStrategy strategy) {
        this.strategy = strategy;
    }

    public void pay(){
        strategy.pay();
    }
}
