package com.company;

public interface PaymentStrategy {
    void pay();
}
