package com.company;

import com.company.Factory.*;
import com.company.Observer.Customer;
import com.company.Observer.IObservable;
import com.company.Observer.Shop;
import com.company.facade.OrderFacade;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Main {
//    private static Map<Integer, Integer> priceOnProducts = new HashMap<>();
//
//    static {
//        priceOnProducts.put(1, 2000);
//        priceOnProducts.put(2, 2500);
//        priceOnProducts.put(3, 1780);
//        priceOnProducts.put(4, 1100);
//    }

    private static Dialog dialog;
    private static Item item;

    public static void main(String[] args) {
        OrderFacade facade=new OrderFacade();
        Scanner scanner_name = new Scanner(System.in);
        System.out.println("Hello, what is your name?");
        String name = scanner_name.nextLine();
        Scanner scanner_phone = new Scanner(System.in);
        System.out.println("Please enter your phone number");
        String phone = scanner_phone.nextLine();
        Scanner scanner_address = new Scanner(System.in);
        System.out.println("Where you from?");
        String address = scanner_address.nextLine();
        // System.out.println("Adidas products: 10** \n Nike products: 20** ");

        Scanner scanner=new Scanner(System.in);
        System.out.println("1 - Adidas\n2 - Nike");
        System.out.println("What do you want to subscribe to?");
        Integer brand=scanner.nextInt();

        choose(brand);
        runCustomer();

        Shop clothesShop = new Shop();
        Customer customer = new Customer(name, phone, address, item);
        clothesShop.registerUser(customer);


        //1 - adidas
        //2 - nike
        Integer brandItem= 1;
        choose(brandItem);
        run(brand,brandItem);

        clothesShop.newItems(item, "shoes");

        Scanner scan = new Scanner(System.in);
        System.out.println("Do you want to order shoes?");
        if(scan.nextLine().equalsIgnoreCase("no")){
            System.exit(0);
        }
        else {
            facade.order();
        }



    }

    static void choose(Integer brand) {
        if (brand==1) {
            dialog = new AdidasDialog();
        } else if (brand==2) {
            dialog = new NikeDialog();
        }
    }

    static void run(Integer brand, Integer brandItem) {
        if(brand!=brandItem)
        item= dialog.createItem();
    }

    static void runCustomer(){
        item= dialog.createItem();
    }
}
